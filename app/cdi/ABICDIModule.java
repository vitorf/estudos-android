package cdi;

import cdi.service.IBeerService;
import cdi.service.ICupService;
import cdi.service.impl.BeerServiceImpl;
import cdi.service.impl.CupServiceImpl;
import com.google.inject.AbstractModule;

/**
 * Created by vitorf on 20/01/2017.
 */
public class ABICDIModule extends AbstractModule {
    /**
     * CDI Module
     */
    @Override
    protected void configure() {
        bind(IBeerService.class).to(BeerServiceImpl.class).asEagerSingleton();
        bind(ICupService.class).to(CupServiceImpl.class).asEagerSingleton();
    }
}
