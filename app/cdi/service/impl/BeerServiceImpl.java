package cdi.service.impl;

import cdi.service.IBeerService;
import com.google.inject.Singleton;

/**
 * Created by vitorf on 20/01/2017.
 */

@Singleton
public class BeerServiceImpl implements IBeerService {
    /**
     *
     * @return
     */
    @Override
    public String retrieveBeerName() {
        return "Czechvar";
    }
}
