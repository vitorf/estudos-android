package cdi.service.impl;

import cdi.service.IBeerService;
import cdi.service.ICupService;
import com.google.inject.Inject;

/**
 * Created by vitorf on 20/01/2017.
 */
public class CupServiceImpl implements ICupService {


    private final IBeerService beerService;

    @Inject
    public CupServiceImpl(final IBeerService beerService) {
                this.beerService = beerService;


    }
    @Override
    public String retrieveBeerData() {
        return beerService.retrieveBeerName();
    }
}
