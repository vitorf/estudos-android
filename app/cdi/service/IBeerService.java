package cdi.service;

/**
 * Created by vitorf on 20/01/2017.
 */
public interface IBeerService {
    String retrieveBeerName();
    
}
