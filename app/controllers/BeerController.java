package controllers;

import cdi.ABICDIModule;
import cdi.service.IBeerService;
import cdi.service.ICupService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import javassist.bytecode.LineNumberAttribute;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by vitorf on 20/01/2017.
 */
public class BeerController extends Controller {

    private ICupService beerService = createInjector();

    public Result getAllBeers(String id) {
        return ok("Ok!! -> "+ beerService.retrieveBeerData());
    }

    public Result error()
    {
        return notFound("abc");
    }

    public Result test()
    {
        return created("Created");
    }

    public ICupService createInjector() {
        Injector injector = Guice.createInjector(new ABICDIModule());
        return injector.getInstance(ICupService.class);
    }
}
